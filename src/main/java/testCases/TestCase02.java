package testCases;

import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import pages.HomePage;
import wdMethods.ProjectMethods;

public class TestCase02 extends ProjectMethods{

	@BeforeTest
	public void setData() {
		testCaseName = "TestCase02";
		testDescription = "Flipkart";
		authors = "Divya";
		category = "smoke";
		dataSheetName = "TC002_CreateLead";
		testNodes = "Flipkart-Advanced";
	}
	@Test
	public void testCase02() {
		new HomePage()
		.clickElectronics()
		.clickMi()
		.verifyPageTitle()
		.clickNewestFirst()
		.printProductwithPrice()
		.clickFirstProduct()
		.verifyNamewithTitle()
		.printRatingsReviews();	
		//System.out.println("₹");
	}
}