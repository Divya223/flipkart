package pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import cucumber.api.java.en.Given;
import wdMethods.ProjectMethods;

public class FirstProductPage extends ProjectMethods{

	public FirstProductPage() {
		PageFactory.initElements(driver, this);
	}
	@FindBy(xpath="(//div[@class='col-12-12']/span)[1]") WebElement ratings;
	@FindBy(xpath="(//div[@class='col-12-12']/span)[2]") WebElement reviews;

	@Given("verify product Name with Title")
	public FirstProductPage verifyNamewithTitle() {
		String title2 = driver.getTitle();
		System.out.println(title2);
		if(title2.contains(NewestPage.firstPrdName)) {
			System.out.println("Title verified");
		}
		else System.out.println("Title not matching");
		return this;
	}
	@Given("print Ratings & Reviews")
	public FirstProductPage printRatingsReviews() {
		System.out.println("Ratings:"+ratings.getText()+" Reviews:"+reviews.getText());
		return this;
	}
}
