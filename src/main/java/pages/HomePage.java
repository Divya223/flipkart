package pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import cucumber.api.java.en.Given;
import wdMethods.ProjectMethods;

public class HomePage extends ProjectMethods{

	public HomePage() {
		PageFactory.initElements(driver, this);
	}

	@FindBy(xpath="//span[text()='Electronics']") WebElement electronics;
	@FindBy(linkText="Mi") WebElement mi;

	@Given("Move over to Electronics")
	public HomePage clickElectronics() {
		Actions builder = new Actions(driver);
		builder.moveToElement(electronics).perform();
		return this;
	}
	@Given("click Mi")
	public MiPage clickMi() {
		click(mi);
		try {
			Thread.sleep(1000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return new MiPage();
	}
}
