package pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import cucumber.api.java.en.Given;
import wdMethods.ProjectMethods;

public class MiPage extends ProjectMethods{
	
	public MiPage() {
		PageFactory.initElements(driver, this);
	}
	
	@FindBy(xpath="//div[text()='Newest First']") WebElement newestFirst;

	@Given("verify PageTitle with product name")
	public MiPage verifyPageTitle() {
		try {
			Thread.sleep(1000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		String title = driver.getTitle();
		System.out.println(title);
		if(title.contains("Mi")) {
			System.out.println("True");  
		}
		else System.out.println("False");
		return this;
	}
	@Given("click Newest First tab")
	public NewestPage clickNewestFirst() {
		click(newestFirst);
		return new NewestPage();
	}
}
