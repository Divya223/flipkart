package pages;

import java.util.List; 

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import cucumber.api.java.en.Given;
import wdMethods.ProjectMethods;

public class NewestPage extends ProjectMethods{

	public NewestPage() {
		PageFactory.initElements(driver, this);
	}
	@FindBy(className="_3wU53n") List<WebElement> productName;
	@FindBy(xpath="//div[@class='_6BWGkk']/child::div[1]/child::div[1]") List<WebElement> allPrices;
	@FindBy(xpath="(//div[@class='_3wU53n'])[1]") WebElement firstPrd;
	static String firstPrdName;
	
	@Given("print Product with Price")
	public NewestPage printProductwithPrice() {
		for(int i=0;i<productName.size();i++) {
			System.out.println(productName.get(i).getText()+"-"+allPrices.get(i).getText());
		}
		return this;
	}
	@Given("click First Product")
	public FirstProductPage clickFirstProduct() {		
		firstPrdName = firstPrd.getText();
		click(firstPrd);
		switchToWindow(1);
		return new FirstProductPage();
	}
}
