package basic;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;
import org.testng.annotations.Test;

public class TestCase01 {

	public static ChromeDriver driver;

	//	public static void main(String[] args) throws InterruptedException {
	@Test
	public void runner() throws InterruptedException {
		System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");
		driver = new ChromeDriver();
		driver.get("https://www.flipkart.com");
		driver.manage().window().maximize();
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);

		driver.getKeyboard().sendKeys(Keys.ESCAPE);

		Actions builder = new Actions(driver);
		WebElement Electronics = driver.findElementByXPath("//span[text()='Electronics']");
		builder.moveToElement(Electronics).perform();

		driver.findElementByLinkText("Mi").click();
		Thread.sleep(3000);
		String title = driver.getTitle();
		System.out.println(title);
		if(title.contains("Mi")) {
			System.out.println("True");
		}
		else System.out.println("False");
		driver.findElementByXPath("//div[text()='Newest First']").click();
		//	int index;
		Thread.sleep(3000);
		List<WebElement> productName = driver.findElementsByClassName("_3wU53n");

		/*for(WebElement prName:productName) {
			System.out.println(prName.getText());
		}*/

		List<WebElement> allPrices = driver.findElementsByXPath("//div[@class='_6BWGkk']/child::div[1]/child::div[1]");

		/*for(WebElement prPrice: allPrices) {
			System.out.println(prPrice.getText());
		}*/

		for(int i=0;i<productName.size();i++) {
			System.out.println(productName.get(i).getText()+"-"+allPrices.get(i).getText());
		}

		String firstPrdName = driver.findElementByXPath("(//div[@class='_3wU53n'])[1]").getText();
		System.out.println(firstPrdName);
		driver.findElementByXPath("(//div[@class='_3wU53n'])[1]").click();
		Thread.sleep(3000);

		Set<String> allWindows = driver.getWindowHandles();
		List<String> allWinList = new ArrayList<String>();
		allWinList.addAll(allWindows);
		driver.switchTo().window(allWinList.get(1));		

		String title2 = driver.getTitle();
		System.out.println(title2);
		if(title2.contains(firstPrdName)) {
			System.out.println("Title verified");
		}
		else System.out.println("Title not matching");

		String ratings = driver.findElementByXPath("(//div[@class='col-12-12']/span)[1]").getText();
		String reviews = driver.findElementByXPath("(//div[@class='col-12-12']/span)[2]").getText();		
		System.out.println("Ratings:"+ratings+" Reviews:"+reviews);
		
		driver.close();
	}
}
//}
